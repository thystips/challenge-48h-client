# CHANGELOG

<!--- next entry here -->

## 1.0.0
2020-05-15

### Fixes

- **environnement prod:** ;) (a42c524bc89092df880ef13e3cf60329c505478d)

## 0.5.0
2020-05-15

### Features

- **app composant:** quelques modifications. (218eb248e4ffadc4e750a32a1e1a2ed241f689bf)
- **search composant:** suppression du componant de recherche pour le faire fusionner. (7698a67e63b823da2d63be416ea62447b18645fa)
- **restaurantservice & composant principal:** ajout du service de communication pour l'api Restaurant. (3173fb9de7421a00f0190ab65658a6d2528ac26d)
- **restaurant card composant:** ajout du Composant restaurant card. (08e546c53e9358dc47ceb3797f003dfed1ce15ca)
- **restaurant page composant:** ajout du Composant restaurant page. (f9a783cb447fa9267d3b1942a2a13a1ae50d3ed8)

## 0.4.0
2020-05-15

### Features

- **header componsant:** Condition si utilisateur connecté ou non. (2afd20566ecd509305b0781c1e9a4c2103e7ed5c)
- **search composant:** Finsalisation du composant de recherche global. (bfa35dd76a04fbadee95754d5a5cde5f1fe1219f)

## 0.3.0
2020-05-15

### Features

- **deployment:** Update compose file on pipeline (e4b730dee40495dd07e223e7d23a95185d6861b3)

## 0.2.1
2020-05-15

### Fixes

- **ci:** force prunning images (a32f53d3df4b3b575c4bb2529b0039c69e7c49f1)

## 0.2.0
2020-05-15

### Features

- **styles:** Ajout de bootstrap et theming du framework. (916aa9ed0148c9c71a2709a190870408ba64aa3b)
- **footer&header composant:** Ajout et finalisation du footer et du header. (19e28c6df4b33c8390572e9e66df3d0fc10db97f)
- **login&resgister composants:** Ajout et finalisation des formulaires de Connexion et d'Inscription. (cbc3571914c353c217fc00851039a8845481b1f7)
- **auth service:** Ajout et finalisation du service d'authentification. (9b05cdb64bbae5d752bf9e0cd738a009fe41bfc9)

### Fixes

- **ci:** Using only prod dependencies (1dd2a9ae6f453bac96f056afe3a030260517e05f)
- **ci:** Use lastest angular cli version (b2b636a33bdde0d0947c2308e0b02f7350665e80)
- **dependencies:** Update dependencies (044c729d49a64b7e1dffadc260c57f9541bd4b5c)
- **dependencies:** Update angular (5be1486c9c6ddc26b352d12f239631ff55e1d23b)
- **client:** initialisation du projet. (0094f994afe81e4fa3af4b98adb449e1d91726d0)
- **npm:** Ajouts de packets NPM. (f670cc7651e8021d54e774da521c9101fb36dce6)
- **npm merge:** conflit sur les packets. (7897e337f499313d69d82e867ed6b1415579424f)
- **merge npm:** Confilts package.json (7259bc845e8ed6de6a53da8f77f3c1f1c16973d8)
- **npm rebase:** Conflit... (e065fdb01852ecc0168d300478c4cbb179ff7890)

### Other changes

- version bump for v0.1.0 [skip ci] (3689af7245ca284edd8442a47ee1b0e78b05d6a1)

## 0.1.0
2020-05-14

### Features

- **ci:** Add Gitlab CI for Build and Deploy (2f06eb3764646bda3b01258651e9a7d38dd92d06)
- **container:** Add Dockerfile (f1a4d2baec9463d1b0ffedc1d12ffa573b4e4a6f)
- **licence:** Add Open Source licence (569542ed7f8a6565b69cc4f22c1d3ccea6db6c83)

### Fixes

- **vscode:** Add workspace (566baa2469f9d5a1f28f6c8d30c75f1860584713)
- **ci:** repair missing build stages (3a7c87483c370c1fb10b924f35c4a4ee3faa4b8e)
- **ci:** Change copy folder (1bcd0f3cdac886fbc518a0130e8721e2a04a5dd1)
- **ci:** Using only prod dependencies (1dd2a9ae6f453bac96f056afe3a030260517e05f)
- **ci:** Use lastest angular cli version (b2b636a33bdde0d0947c2308e0b02f7350665e80)
- **dependencies:** Update dependencies (044c729d49a64b7e1dffadc260c57f9541bd4b5c)
- **dependencies:** Update angular (5be1486c9c6ddc26b352d12f239631ff55e1d23b)

### Other changes

- Initial commit (7a21ed2c8d62d0c90fb1834673149dadc0256138)
- version bump for v0.1.0 [skip ci] (07bf089af5f1932760810d7e4cef68ad73f53816)

## 0.1.0
2020-05-14

### Features

- **ci:** Add Gitlab CI for Build and Deploy (2f06eb3764646bda3b01258651e9a7d38dd92d06)
- **container:** Add Dockerfile (f1a4d2baec9463d1b0ffedc1d12ffa573b4e4a6f)
- **licence:** Add Open Source licence (569542ed7f8a6565b69cc4f22c1d3ccea6db6c83)

### Fixes

- **vscode:** Add workspace (566baa2469f9d5a1f28f6c8d30c75f1860584713)
- **ci:** repair missing build stages (3a7c87483c370c1fb10b924f35c4a4ee3faa4b8e)
- **ci:** Change copy folder (1bcd0f3cdac886fbc518a0130e8721e2a04a5dd1)

### Other changes

- Initial commit (7a21ed2c8d62d0c90fb1834673149dadc0256138)
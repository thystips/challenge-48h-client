import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(public auth: AuthService) {
    }

    ngOnInit(): void {
    }

    logout() {
        this.auth.logout().subscribe(
            value => {
                location.reload();
            },
            error => {
                console.log(error)
            }
        )
    }

}

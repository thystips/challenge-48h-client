import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RestaurantService} from "../../services/restaurant/restaurant.service";

@Component({
    selector: 'app-restaurant-page',
    templateUrl: './restaurant-page.component.html',
    styleUrls: ['./restaurant-page.component.scss']
})
export class RestaurantPageComponent implements OnInit {

    response: any;
    avis: any;

    restaurantId = this.router.snapshot.paramMap.get("id");

    constructor(private router: ActivatedRoute, private restaurantService: RestaurantService) {
    }

    ngOnInit(): void {
        this.restaurantService.getOne(this.restaurantId).subscribe(
            value => {
                this.response = value.data;
            },
            error => {
                console.log(error);
            },
            () => {
            }
        );
        this.restaurantService.getAvis(this.restaurantId).subscribe(
            value => {
                this.avis = value.data;
                console.log(this.avis);
            },
            error => {
                console.log(error)
            },
            () => {
            }
        )
    }


}

import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-restaurant-card',
    templateUrl: './restaurant-card.component.html',
    styleUrls: ['./restaurant-card.component.scss']
})
export class RestaurantCardComponent implements OnInit {

    @Input() nom;
    @Input() note;
    @Input() adresse;
    @Input() type;
    @Input() prixMoyen;
    @Input() restoId;

    constructor(private router: Router) {
    }

    ngOnInit(): void {


    }

    navigate() {
        this.router.navigateByUrl('/restaurant/' + this.restoId);
    }

}

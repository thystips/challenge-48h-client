import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpParams} from "@angular/common/http";
import {AuthService} from "../../services/auth/auth.service";

declare let $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    response: any;
    buttonLoading: boolean = false;

    constructor(private fb: FormBuilder, private auth: AuthService) {
    }

    ngOnInit(): void {

        this.loginForm = this.fb.group({
            username: ['',
                [
                    Validators.required
                ]
            ],
            password: ['',
                [
                    Validators.required
                ]
            ]
        });

    }

    get form() {
        return this.loginForm.controls;
    }

    get formEncoded() {
        return new HttpParams()
            .set('username', this.form.username.value)
            .set('password', this.form.password.value)
            .toString();
    }

    submit() {

        this.btnToggleLoader();
        this.auth.login(this.formEncoded).subscribe(
            value => {
                console.log(value);
                this.auth.manualSetUser(value.user);
                this.response = value;
            },
            error => {
                this.response = error;
            },
            () => {
                this.btnToggleLoader();
            }
        );

    }

    btnToggleLoader() {
        if (!this.buttonLoading) {
            $('button.col-sm-6.form-control.btn.btn-dark').html(`

            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>

            `);
            this.buttonLoading = !this.buttonLoading;
        } else {
            $('button.col-sm-6.form-control.btn.btn-dark').html(`Connexion`);
            this.buttonLoading = !this.buttonLoading;
        }
    }

}

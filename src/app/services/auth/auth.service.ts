import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    user: any;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    };

    constructor(private http: HttpClient) {

        this.loggedUser().subscribe(value => {
                if (value.code === 200) {
                    this.user = value.data;
                    localStorage.setItem('user', JSON.stringify(value.data));
                }
            },
            error => {
                if (error.error.code === 401 || error.error.code === 404) {
                    localStorage.removeItem('user');
                }
            }
        );

    }

    get = (userId: string): Observable<any> => {
        return this.http.get(environment.Api.url + 'user/' + userId, this.httpOptions);
    };

    login(val: any): Observable<any> {
        return this.http.post(environment.Api.url + 'user/login', val, this.httpOptions);
    }

    manualSetUser(val: any) {
        localStorage.setItem('user', val);
    }

    loggedUser(): Observable<any> {
        return this.http.get(environment.Api.url + 'user/current');
    }

    get isLoggedIn() {
        return !!localStorage.getItem('user');
    }

    register(val: any): Observable<any> {
        return this.http.post(environment.Api.url + 'user/register', val, this.httpOptions);
    }

    logout(): Observable<any> {
        return this.http.get(environment.Api.url + 'user/logout', this.httpOptions);
    }

}

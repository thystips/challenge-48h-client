import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class RestaurantService {

    constructor(private http: HttpClient) {
    }

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    };

    getAll(search = '', limit = 20, offset = 0): Observable<any> {
        return this.http.get(environment.Api.url + 'restaurant?limit=' + limit + '&offset=' + offset + '&s=' + search, this.httpOptions);
    }

    getOne(restaurantId): Observable<any> {
        return this.http.get(environment.Api.url + 'restaurant/' + restaurantId, this.httpOptions);
    }

    getAvis(restaurantId): Observable<any> {
        return this.http.get(environment.Api.url + 'restaurant/' + restaurantId + '/avis', this.httpOptions);
    }

}

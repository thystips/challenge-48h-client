import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RestaurantService} from "./services/restaurant/restaurant.service";

declare let $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    searchForm: FormGroup;
    buttonLoading: boolean = false;
    response: any;
    searchString: string;

    constructor(public router: Router, private fb: FormBuilder, private restaurantService: RestaurantService) {
    }

    ngOnInit(): void {

        this.searchForm = this.fb.group({
            search: ['',
                [
                    Validators.required
                ]
            ]
        });

    }

    get form() {
        return this.searchForm.controls;
    }

    search() {

        this.searchString = this.form.search.value

        this.btnToggleLoader();
        this.restaurantService.getAll(this.searchString).subscribe(
            value => {
                console.log(value);
                this.response = value.data;
            },
            error => {
                this.response = error;
            },
            () => {
                this.btnToggleLoader();
            }
        );

    }

    btnToggleLoader() {
        if (!this.buttonLoading) {
            $('button.btn.btn-primary.col-sm-3').html(`

            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>

            `);
            this.buttonLoading = !this.buttonLoading;
        } else {
            $('button.btn.btn-primary.col-sm-3').html(`Rechercher`);
            this.buttonLoading = !this.buttonLoading;
        }
    }

}
